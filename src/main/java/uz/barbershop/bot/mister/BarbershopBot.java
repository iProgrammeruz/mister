package uz.barbershop.bot.mister;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendContact;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Contact;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.barbershop.bot.mister.entity.BarberConfigs;
import uz.barbershop.bot.mister.entity.Users;
import uz.barbershop.bot.mister.entity.enums.Roles;
import uz.barbershop.bot.mister.repository.BarberConfigRepository;
import uz.barbershop.bot.mister.repository.UserRepository;
import uz.barbershop.bot.mister.utils.UserState;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class BarbershopBot extends TelegramLongPollingBot {

    @Value("$app.token}")
    private String token;

    @Value("$app.username}")
    private String username;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BarberConfigRepository configRepository;


    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasCallbackQuery()) {
            final Long chatId = update.getCallbackQuery().getFrom().getId();
            final Integer msgId = update.getCallbackQuery().getMessage().getMessageId();
            final User tgUser = update.getCallbackQuery().getFrom();
            final String QUERY = update.getCallbackQuery().getData();
            final String queryID = update.getCallbackQuery().getId();

            Optional<Users> currUser = userRepository.findByChatId(chatId);

            if (currUser.isEmpty()) {
                try {
                    execute(AnswerCallbackQuery.builder().callbackQueryId(queryID).text("YOU DON'T HAVE PERMISSION").build());
                } catch (TelegramApiException e) {
                    return;
                }
                return;
            }

            if (Objects.equals(QUERY, "UZ") || Objects.equals(QUERY, "RU")) {
                msgDeleter(chatId, msgId);
                if (Objects.equals(currUser.get().getState(), UserState.NEW)) {
                    Integer currMsgId = getClientPhone(chatId);

                    currUser.get().setLang(QUERY);
                    currUser.get().setState(UserState.CONTACT);
                    currUser.get().setTmp(String.valueOf(currMsgId));
                    userRepository.save(currUser.get());
                    return;
                }
                currUser.get().setLang(QUERY);
                currUser.get().setState(UserState.HOME);
                userRepository.save(currUser.get());
                getHome(chatId, currUser.get().getRole());
                return;

            } else if (QUERY.startsWith("BARBER_ID@")) {

                return;

            } else if (Objects.equals(QUERY, "BACK") || Objects.equals(QUERY, "HOME")) {
                getHome(chatId, currUser.get().getRole());
                msgDeleter(chatId, msgId);
                return;
            } else if (Objects.equals(QUERY, "BARBER_LIST")) {
                getAllBarbers(chatId);
                msgDeleter(chatId, msgId);
                return;
            } else if (Objects.equals(QUERY, "LANG_CHANGE")) {
                chooseLang(chatId);
                msgDeleter(chatId, msgId);
                return;
            }

            if (Objects.equals(currUser.get().getRole(), Roles.ADMIN)) {
                switch (QUERY) {
                    case "ADD_BARBER":

                        break;
                }
                return;
            }
            if (Objects.equals(currUser.get().getRole(), Roles.BARBER)) {

                return;
            }

        } else if (update.hasMessage()) {
            final String msg = Objects.nonNull(update.getMessage()) ? update.getMessage().getText() : "";
            final Long chatId = update.getMessage().getChatId();
            final Integer msgId = update.getMessage().getMessageId();
            final User tgUser = update.getMessage().getFrom();

            Optional<Users> currUser = userRepository.findByChatId(chatId);

            if (Objects.equals(msg, "/start")) {
                chooseLang(chatId);

                if (currUser.isEmpty()) {
                    Users newUser = new Users();
                    newUser.setChatId(chatId);
                    newUser.setState(UserState.NEW);
                    newUser.setFirstName(tgUser.getFirstName());
                    if (Objects.nonNull(tgUser.getLastName())) {
                        newUser.setLastName(tgUser.getLastName());
                    }
                    if (Objects.nonNull(tgUser.getUserName())) {
                        newUser.setUsername(tgUser.getUserName());
                    }
                    userRepository.save(newUser);
                }

                return;
            }

            if (update.getMessage().hasContact() && currUser.isPresent() && Objects.equals(currUser.get().getState(), UserState.CONTACT)) {
                msgDeleter(chatId, msgId);
                String tmp = currUser.get().getTmp();
                Contact c = update.getMessage().getContact();
                currUser.get().setPhoneNumber(c.getPhoneNumber());
                currUser.get().setState(UserState.HOME);
                currUser.get().setTmp("");
                userRepository.save(currUser.get());

                getAllBarbers(chatId);
                try {
                    int chID = Integer.parseInt(tmp);
                    msgDeleter(chatId, chID);
                } catch (Exception e) {
                    return;
                }
                return;
            }

            if (currUser.isPresent() && Objects.equals(currUser.get().getRole(), Roles.ADMIN)) {

            }
            if (currUser.isPresent() && Objects.equals(currUser.get().getRole(), Roles.BARBER)) {

            }
        }
    }


    /**
     * Logical Functions
     */

    private void chooseLang(Long chatId) {
        InlineKeyboardMarkup inlineLangKeyMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton uzLangKey = new InlineKeyboardButton();
        InlineKeyboardButton ruLangKey = new InlineKeyboardButton();
        uzLangKey.setText("\uD83C\uDDFA\uD83C\uDDFF O'zbek");
        ruLangKey.setText("\uD83C\uDDF7\uD83C\uDDFA Russian");
        uzLangKey.setCallbackData("UZ");
        ruLangKey.setCallbackData("RU");

        inlineLangKeyMarkup.setKeyboard(Arrays.asList(Arrays.asList(uzLangKey, ruLangKey)));
        String msg = "Choose lang";
        msgSender(chatId, msg, inlineLangKeyMarkup);
    }

    private Integer getClientPhone(Long chatId) {
        String msg = "PLS SEND CONTACT";

        ReplyKeyboardMarkup k1 = new ReplyKeyboardMarkup();
        k1.setResizeKeyboard(true);
        List<KeyboardRow> rows = new ArrayList<>();
        KeyboardRow keyboard1 = new KeyboardRow();

        KeyboardButton btn = new KeyboardButton();
        btn.setRequestContact(true);
        btn.setText("SEND MY CONTACT");
        keyboard1.add(btn);

        rows.add(keyboard1);
        k1.setKeyboard(rows);

        return msgSenderGetMsgId(chatId, msg, k1);

    }

    private void getAllBarbers(Long chatId) {
        List<Users> allBarbers = userRepository.findAllByRole(Roles.BARBER);
        InlineKeyboardMarkup inlineLangKeyMarkup = new InlineKeyboardMarkup();
        InlineKeyboardButton backKey = new InlineKeyboardButton();
        backKey.setText(UserState.BACK);
        backKey.setCallbackData(UserState.BACK);

        if (Objects.equals(allBarbers.size(), 0)) {

            inlineLangKeyMarkup.setKeyboard(Arrays.asList(Arrays.asList(backKey)));
            msgSender(chatId, "NO BARBERS", inlineLangKeyMarkup);
            return;
        }


        List<List<InlineKeyboardButton>> barberList = allBarbers.stream().map(b -> {
            InlineKeyboardButton inlineKey = new InlineKeyboardButton();
            inlineKey.setText(b.getFirstName());
            inlineKey.setCallbackData("BARBER_ID@" + b.getChatId());
            return Arrays.asList(inlineKey);
        }).collect(Collectors.toList());
        barberList.add(Arrays.asList(backKey));

        inlineLangKeyMarkup.setKeyboard(barberList);
        String msg = "Choose one of the Barber";
        msgSender(chatId, msg, inlineLangKeyMarkup);

    }

    private void getHome(Long chatId, Roles role) {
        InlineKeyboardMarkup inlineLangKeyMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton barbersKey = new InlineKeyboardButton();
        barbersKey.setText("Barber List");
        barbersKey.setCallbackData("BARBER_LIST");

        InlineKeyboardButton changeLangKey = new InlineKeyboardButton();
        changeLangKey.setText("Change Lang");
        changeLangKey.setCallbackData("LANG_CHANGE");

        List<List<InlineKeyboardButton>> keyboardButtons = new ArrayList<>();
        keyboardButtons.add(List.of(barbersKey));
        keyboardButtons.add(List.of(changeLangKey));

        if (Objects.equals(role, Roles.ADMIN)) {
            InlineKeyboardButton addBarber = new InlineKeyboardButton();
            addBarber.setText("Add Barber");
            addBarber.setCallbackData("ADD_BARBER");
            keyboardButtons.add(List.of(addBarber));
        }
        if (Objects.equals(role, Roles.BARBER)) {
            InlineKeyboardButton cfg = new InlineKeyboardButton();
            cfg.setText("my configs");
            cfg.setCallbackData("CFG");
            keyboardButtons.add(List.of(cfg));
        }

        inlineLangKeyMarkup.setKeyboard(keyboardButtons);
        String msg = "WELCOME !";
        msgSender(chatId, msg, inlineLangKeyMarkup);

    }

    private void setDefaultBarberConfigs(Users user) {
        try {
            BarberConfigs cfg = new BarberConfigs();
            cfg.setStartHour(9);
            cfg.setStartMinute(0);
            cfg.setEndHour(20);
            cfg.setEndMinute(0);
            cfg.setHourBetween(30L);
            cfg.setLaunchTimeDuration(60L);
            cfg.setUser(user);

            configRepository.save(cfg);

        } catch (Exception e) {

        }

    }


    //  ================ COMMON FUNCTIONS ==================

    private void msgDeleter(Long chatId, Integer msgId) {
        try {
            DeleteMessage dM = new DeleteMessage();
            dM.setChatId(String.valueOf(chatId));
            dM.setMessageId(msgId);
            execute(dM);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void msgUpdater(Long chatId, Integer msgId, String msg) {
        try {
            EditMessageText editMsg = new EditMessageText();
            editMsg.setText(msg);
            editMsg.setChatId(String.valueOf(chatId));
            editMsg.setMessageId(msgId);
            execute(editMsg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void msgUpdater(Long chatId, Integer msgId, String msg, InlineKeyboardMarkup key) {
        try {
            EditMessageText editMsg = new EditMessageText();
            editMsg.setChatId(String.valueOf(chatId));
            editMsg.setMessageId(msgId);
            editMsg.setText(msg);
            editMsg.setReplyMarkup(key);
            execute(editMsg);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void msgSender(Long chatId, String msg) {
        try {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setText(msg);
            sendMessage.setChatId(String.valueOf(chatId));
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void msgSender(Long chatId, String msg, ReplyKeyboard key) {
        try {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setText(msg);
            if (Objects.nonNull(key)) {
                sendMessage.setReplyMarkup(key);
            }
            sendMessage.setChatId(String.valueOf(chatId));
            Message m = execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private Integer msgSenderGetMsgId(Long chatId, String msg, ReplyKeyboard key) {
        try {
            SendMessage sendMessage = new SendMessage();
            sendMessage.setText(msg);
            if (Objects.nonNull(key)) {
                sendMessage.setReplyMarkup(key);
            }
            sendMessage.setChatId(String.valueOf(chatId));
            Message m = execute(sendMessage);
            return m.getMessageId();
        } catch (TelegramApiException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public String getBotUsername() {
        // TODO
        return this.username;
    }

    @Override
    public String getBotToken() {
        // TODO
        return "2017542435:AAHMkIAZfiK3fITFB5LNjHQ6skr7oOcn7zg";
    }


}
