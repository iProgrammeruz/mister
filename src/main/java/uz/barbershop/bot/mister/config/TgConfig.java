package uz.barbershop.bot.mister.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import uz.barbershop.bot.mister.BarbershopBot;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class TgConfig {

    @Autowired
    private BarbershopBot bot;

    @PostConstruct
    protected void init() {
        //инициализируйте конфигурацию здесь
        try {
            System.out.println("CONF");
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);

            botsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            /// it's your exception
        } finally {
        }
    }

    @PreDestroy
    protected void closeSession() {
        //де-инициализируйте конфигурацию здесь если есть такая необходимость
        //например закройте connection если таковой имеется
    }

}
