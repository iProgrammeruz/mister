package uz.barbershop.bot.mister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.barbershop.bot.mister.entity.*;
import uz.barbershop.bot.mister.entity.enums.OrderStatus;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Orders, UUID> {

    List<Orders> findAllByBarber(Users barber);

    List<Orders> findAllByUser(Users user);

    List<Orders> findAllByDay(Days day);

    List<Orders> findAllByStatus(OrderStatus status);



}
