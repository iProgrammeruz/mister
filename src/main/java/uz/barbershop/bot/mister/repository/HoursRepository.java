package uz.barbershop.bot.mister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.barbershop.bot.mister.entity.Hours;

import java.util.List;
import java.util.UUID;

@Repository
public interface HoursRepository extends JpaRepository<Hours, UUID> {


    List<Hours> findAllByIsOrdered(Boolean isOrdered);

}
