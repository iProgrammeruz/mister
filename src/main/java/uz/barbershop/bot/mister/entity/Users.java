package uz.barbershop.bot.mister.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.barbershop.bot.mister.entity.enums.Roles;

import javax.persistence.*;
import javax.validation.constraints.Size;


//@EqualsAndHashCode(callSuper = true)
@Data
//@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(
        value = {"password", "address", "deals", "accountNonExpired", "accountNonLocked", "credentialsNonExpired", "enabled"},
        allowGetters = true, allowSetters = true)
@Entity(name = "users")
public class Users extends AbsEntity {

    @Column(unique = true, nullable = false)
    private Long chatId;

    @Column(unique = true, length = 50)
    private String username;

    @Column(unique = true, length = 13)
    @Size(min = 12, max = 13)
    private String phoneNumber;

    @Column(nullable = false, length = 50)
    private String firstName;

    @Column(length = 50)
    private String lastName;

    @Column(length = 5)
    private String lang;

    @Column
    private String state;

    @Column
    private String tmp;

    @Enumerated(EnumType.STRING)
    private Roles role;

    public Users() {
        this.role = Roles.USER;
        this.state = "NEW";
    }
}
