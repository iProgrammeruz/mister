package uz.barbershop.bot.mister.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "days")
public class Days extends AbsEntity {

    @Column(length = 50)
    private String name;

    @Column
    private Long month;

    @Column
    private Integer value;

    @Column
    private Boolean isOpen = false;

    @ManyToOne(fetch = FetchType.LAZY)
    private Users user;



}
