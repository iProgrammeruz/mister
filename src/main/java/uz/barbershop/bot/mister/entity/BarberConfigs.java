package uz.barbershop.bot.mister.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "barber_configs")
public class BarberConfigs extends AbsEntity {

    @OneToOne(fetch = FetchType.LAZY)
    private Users user;

    @Column(length = 3)
    private Integer startHour; // 9
    @Column(length = 3)
    private Integer startMinute; // 0

    @Column(length = 3)
    private Integer endHour; // 20
    @Column(length = 3)
    private Integer endMinute; // 0

    @Column(length = 3)
    private Long hourBetween; // 60
    @Column(length = 3)
    private Long launchTimeDuration; // 60

}
